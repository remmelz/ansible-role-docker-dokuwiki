#!/bin/bash

###################
apt-get update
apt-get -y upgrade
apt-get -y install \
  wget \
  nginx \
  php \
  php-gd \
  php-fpm \
  php-xml
mkdir -p /var/run/php
chown www-data. /var/run/php
cd /var/run/php
ln -s php7.4-fpm.sock php-fpm.sock
###################
cd /srv
###################
wget https://download.dokuwiki.org/src/dokuwiki/dokuwiki-stable.tgz
tar xzf dokuwiki-stable.tgz
rm *.tgz
mv * dokuwiki
tar xf /var/tmp/templates/bootstrap3.tar.gz -C /srv/dokuwiki/lib/tpl/
###################
cp -v /var/tmp/nginx.conf /etc/nginx/sites-available/dokuwiki.conf
cd /etc/nginx/sites-enabled
rm default
ln -s ../sites-available/dokuwiki.conf

